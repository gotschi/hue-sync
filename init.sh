#!/bin/sh

#chmod 0777 /tmp/fifo
FILE=/app/config/config.json
if ! test -f "$FILE"; then
    TS_NODE_FILES=true TS_NODE_TRANSPILE_ONLY=true NODE_TLS_REJECT_UNAUTHORIZED=0 ts-node setup.ts
    #NODE_NO_WARNINGS=1 NODE_TLS_REJECT_UNAUTHORIZED=0 npx ts-node --esm setup.ts
else
    TS_NODE_FILES=true TS_NODE_TRANSPILE_ONLY=true NODE_TLS_REJECT_UNAUTHORIZED=0 ts-node index.ts
    #NODE_NO_WARNINGS=1 NODE_TLS_REJECT_UNAUTHORIZED=0 npx ts-node --esm index.ts
fi
