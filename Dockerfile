FROM node:alpine

COPY app /app
WORKDIR /app

RUN npm install hue-sync
RUN npm install -g ts-node

CMD ["NODE_TLS_REJECT_UNAUTHORIZED=0", "npx", "ts-node", "--esm", "index.ts"]
