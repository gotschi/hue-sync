import HueSync from "hue-sync";
const fs = require('fs')
const path = require('path');

var connected = false;

(async function () {

  var data = fs.readFileSync('/app/config/config.json');
  var credentials = JSON.parse(data);
  
  console.log(credentials);

  const bridge = await HueSync.discover();
  const fd = fs.openSync('/tmp/fifo', 'r+')
  const stream = fs.createReadStream(null, {fd})

  var hueBridge = new HueSync({ credentials, id: credentials.id, url: credentials.ip });

  var selectedArea = await hueBridge.getEntertainmentAreas();
  await hueBridge.start(selectedArea[credentials.area]);
  connected = true;

  stream.on('data',async (d) => {
    var data = d.toString().replace(/}/g, ',').replace(/[^0-9&,]/g,'').split(",");
    var data = data.map(function (x) { 
      return parseInt(x, 10); 
    });
    
    try {
      if(connected) await hueBridge.transition([[data[0],data[1],data[2]],[data[3],data[4],data[5]],[data[6],data[7],data[8]],[data[9],data[10],data[11]],[data[12],data[13],data[14]],[data[15],data[16],data[17]],[data[18],data[19],data[20]]]);
    } catch(e) {
      connected = false;
      hueBridge.stop();
      hueBridge = new HueSync({ credentials, id: credentials.id, url: credentials.ip });

      var selectedArea = await hueBridge.getEntertainmentAreas();
      await hueBridge.start(selectedArea[credentials.area]);
      connected = true;
    }
  })

})()
