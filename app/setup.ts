import HueSync from "hue-sync";
import * as readline from 'readline';

const fs = require('fs')
const path = require('path');
const appName = "hue-sync-new";


var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});



var ip;
var id;
var credentials;
var areas;
var selectedArea;

(async function () {
  const bridge = await HueSync.discover();

  if(bridge.length > 0) {
    console.log("bridge found:");
    console.log(bridge);
    ip = bridge[0].internalipaddress;
    id = bridge[0].id;
    buttonPress();
  }

  else {
    rl1.question("enter IP of bridge manually: \n", function (answer) {
        ip = answer;
        rl1.close();
        buttonPress();
    });
  }
})()

function buttonPress() {
    var rl2 = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    rl2.question("please press the button on the bridge and press [ENTER] \n", function (button) {
        const button2 = button;
        rl2.close();
        init();
    });
}

async function save(a) {
  console.log("saving configuration...")
  credentials.area = a;
  credentials.ip = ip;
  credentials.id = id;
  var text = JSON.stringify(credentials);

  fs.writeFileSync("/app/config/config.json", text, function(err){
    if(err){
      return console.log("error");
      console.log(err);
      }
  })

  console.log("\nFinished.")
}

async function init() {
  console.log("Trying to authenticate against Hue Bridge with IP: "+ip);

  try {
    credentials = await HueSync.register(ip, "test-test");
  } catch (err){
    console.log(err);
  }
  console.log(credentials);

  const hueBridge = new HueSync({
    credentials,
    id: id,
    url: ip,
  });

	areas = await hueBridge.getEntertainmentAreas();
  console.log("\n\nyou have the following entertainment areas: \n")

  var i = 0;
  areas.forEach(function(area) {
    console.log(" "+i+" : "+area.metadata.name);
    i++;
  })

  console.log("\nplease enter the NUMBER for the area with your gradient strip: \n")


  var rl3 = readline.createInterface({
      input: process.stdin
  });

  rl3.question("please enter the NUMBER for the area with your gradient strip: \n", function (c) {
        const selectedArea = c;

        rl3.close();

        save(selectedArea);
  });

}