function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for(var i = 0, arr2 = new Array(len); i < len; i++)arr2[i] = arr[i];
    return arr2;
}
function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
}
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
        var info = gen[key](arg);
        var value = info.value;
    } catch (error) {
        reject(error);
        return;
    }
    if (info.done) {
        resolve(value);
    } else {
        Promise.resolve(value).then(_next, _throw);
    }
}
function _asyncToGenerator(fn) {
    return function() {
        var self = this, args = arguments;
        return new Promise(function(resolve, reject) {
            var gen = fn.apply(self, args);
            function _next(value) {
                asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
            }
            function _throw(err) {
                asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
            }
            _next(undefined);
        });
    };
}
function _iterableToArrayLimit(arr, i) {
    var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];
    if (_i == null) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _s, _e;
    try {
        for(_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true){
            _arr.push(_s.value);
            if (i && _arr.length === i) break;
        }
    } catch (err) {
        _d = true;
        _e = err;
    } finally{
        try {
            if (!_n && _i["return"] != null) _i["return"]();
        } finally{
            if (_d) throw _e;
        }
    }
    return _arr;
}
function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}
function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(n);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
var __generator = this && this.__generator || function(thisArg, body) {
    var f, y, t, g, _ = {
        label: 0,
        sent: function() {
            if (t[0] & 1) throw t[1];
            return t[1];
        },
        trys: [],
        ops: []
    };
    return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
    }, typeof Symbol === "function" && (g[Symbol.iterator] = function() {
        return this;
    }), g;
    function verb(n) {
        return function(v) {
            return step([
                n,
                v
            ]);
        };
    }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while(_)try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [
                op[0] & 2,
                t.value
            ];
            switch(op[0]){
                case 0:
                case 1:
                    t = op;
                    break;
                case 4:
                    _.label++;
                    return {
                        value: op[1],
                        done: false
                    };
                case 5:
                    _.label++;
                    y = op[1];
                    op = [
                        0
                    ];
                    continue;
                case 7:
                    op = _.ops.pop();
                    _.trys.pop();
                    continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                        _ = 0;
                        continue;
                    }
                    if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                        _.label = op[1];
                        break;
                    }
                    if (op[0] === 6 && _.label < t[1]) {
                        _.label = t[1];
                        t = op;
                        break;
                    }
                    if (t && _.label < t[2]) {
                        _.label = t[2];
                        _.ops.push(op);
                        break;
                    }
                    if (t[2]) _.ops.pop();
                    _.trys.pop();
                    continue;
            }
            op = body.call(thisArg, _);
        } catch (e) {
            op = [
                6,
                e
            ];
            y = 0;
        } finally{
            f = t = 0;
        }
        if (op[0] & 5) throw op[1];
        return {
            value: op[0] ? op[1] : void 0,
            done: true
        };
    }
};
import fetchMock from "jest-fetch-mock";
import mdnsMock from "./__mocks__/node-dns-sd";
import HueBridge from "./hue";
// Mocks
fetchMock.enableMocks();
var mockID = "foo-bar";
var mockIp = "1.2.3.4";
var mockCredentials = {
    username: "foo",
    clientkey: "bar"
};
var justGreen = [
    0,
    255,
    0
];
var mockResourceNode = {
    rid: "foo",
    rtype: "_test"
};
var mockLight = {
    alert: {
        action_values: [
            "test"
        ]
    },
    color: {
        gamut: {
            blue: {
                x: 1,
                y: 2
            },
            green: {
                x: 3,
                y: 4
            },
            red: {
                x: 5,
                y: 6
            }
        },
        gamut_type: "test",
        xy: {
            x: 1,
            y: 2
        }
    },
    color_temperature: {
        mirek: 1,
        mirek_schema: {
            mirek_maximum: 10,
            mirek_minimum: 0
        },
        mirek_valid: true
    },
    dimming: {
        brightness: 1,
        min_dim_level: 0
    },
    dynamics: {
        speed: 1,
        speed_valid: true,
        status: "foo",
        status_values: [
            "foo",
            "bar"
        ]
    },
    effects: {
        effect_values: [
            "foo",
            "bar"
        ],
        status: "baz",
        status_values: [
            "baz",
            "bar",
            "foo"
        ]
    },
    gradient: {
        points: [
            {
                color: {
                    xy: {
                        x: 1,
                        y: 2
                    }
                }
            }
        ],
        points_capable: 1
    },
    id: "test",
    id_v1: "test",
    metadata: {
        archetype: "test",
        name: "foobar"
    },
    mode: "test",
    on: {
        on: true
    },
    owner: mockResourceNode,
    type: "test"
};
var secondMockLight = {
    alert: {
        action_values: [
            "test"
        ]
    },
    color: {
        gamut: {
            blue: {
                x: 1,
                y: 2
            },
            green: {
                x: 3,
                y: 4
            },
            red: {
                x: 5,
                y: 6
            }
        },
        gamut_type: "test",
        xy: {
            x: 1,
            y: 2
        }
    },
    color_temperature: {
        mirek: 1,
        mirek_schema: {
            mirek_maximum: 10,
            mirek_minimum: 0
        },
        mirek_valid: true
    },
    dimming: {
        brightness: 1,
        min_dim_level: 0
    },
    dynamics: {
        speed: 1,
        speed_valid: true,
        status: "foo",
        status_values: [
            "foo",
            "bar"
        ]
    },
    effects: {
        effect_values: [
            "foo",
            "bar"
        ],
        status: "baz",
        status_values: [
            "baz",
            "bar",
            "foo"
        ]
    },
    gradient: {
        points: [
            {
                color: {
                    xy: {
                        x: 1,
                        y: 2
                    }
                }
            }
        ],
        points_capable: 1
    },
    id: "test2",
    id_v1: "test2",
    metadata: {
        archetype: "test",
        name: "foobar"
    },
    mode: "test",
    on: {
        on: true
    },
    owner: mockResourceNode,
    type: "test"
};
var mockLightGroup = {
    id: "foo",
    type: "test",
    on: {
        on: true
    },
    alert: {
        action_values: [
            "test"
        ]
    }
};
var mockLightGroup2 = {
    id: "bar",
    type: "baz",
    on: {
        on: false
    },
    alert: {
        action_values: [
            "test"
        ]
    }
};
var mockScene = {
    id: "foo",
    type: "test",
    group: mockResourceNode,
    metadata: {
        name: "bar"
    },
    actions: [
        {
            target: mockResourceNode,
            action: {
                on: {
                    on: true
                }
            }
        }, 
    ],
    palette: {
        color: {
            x: 1,
            y: 0
        },
        dimming: {
            brightness: 100
        },
        color_temperature: [
            {
                color_temperature: {
                    mirek: 300
                }
            }
        ]
    }
};
var mockRoom = {
    id: "bar",
    type: "test",
    children: [
        mockResourceNode
    ],
    grouped_services: [
        mockResourceNode
    ],
    metadata: {
        archetype: "test",
        name: "foo"
    },
    services: [
        mockResourceNode
    ]
};
var mockZone = {
    id: "bar",
    type: "test",
    services: [
        mockResourceNode
    ],
    grouped_services: [
        mockResourceNode
    ],
    children: [
        mockResourceNode
    ],
    metadata: {
        name: "foo"
    }
};
var mockHomeArea = {
    children: [
        mockResourceNode
    ],
    grouped_services: [
        mockResourceNode
    ],
    id: "foobar",
    services: [
        mockResourceNode
    ],
    type: "test"
};
var mockEntertainmentArea = {
    channels: [
        {
            channel_id: 0,
            position: [
                {
                    x: 1,
                    y: 2,
                    z: 3
                },
                {
                    x: 4,
                    y: 5,
                    z: 6
                }, 
            ],
            members: [
                {
                    index: 0,
                    service: mockResourceNode
                }, 
            ]
        }, 
    ],
    configuration_type: "test",
    id: "test",
    id_v1: "test",
    light_services: [
        mockResourceNode
    ],
    locations: {
        service_locations: [
            {
                position: {
                    x: 1,
                    y: 2,
                    z: 3
                },
                positions: [
                    {
                        x: 1,
                        y: 2,
                        z: 3
                    },
                    {
                        x: 4,
                        y: 5,
                        z: 6
                    }, 
                ],
                service: mockResourceNode
            }, 
        ]
    },
    metadata: {
        name: "test"
    },
    name: "test",
    status: "test",
    stream_proxy: {
        mode: "test",
        node: mockResourceNode
    },
    type: "test"
};
var mockGeoFenceClient = {
    id: "baz",
    type: "test",
    name: "foo",
    is_at_home: false
};
var mockBehaviorInstance = {
    id: "bar",
    type: "test",
    script_id: "foo",
    enabled: true,
    state: {},
    configuration: {},
    last_error: "",
    migrated_from: "",
    metadata: {
        name: "baz"
    },
    status: "initializing",
    dependees: [
        {
            type: "test",
            target: mockResourceNode,
            level: "non_critical"
        }, 
    ]
};
var mockDevice = {
    id: "bar",
    type: "test",
    services: [
        mockResourceNode
    ],
    metadata: {
        name: "foo"
    },
    product_data: {
        certified: true,
        manufacturer_name: "acme",
        model_id: "baz",
        product_name: "test",
        software_version: "0.0.0",
        product_archetype: ""
    }
};
var mockDevice2 = {
    id: "foo",
    type: "test",
    services: [
        mockResourceNode
    ],
    metadata: {
        name: "baz"
    },
    product_data: {
        certified: true,
        manufacturer_name: "acme",
        model_id: "baz",
        product_name: "test",
        software_version: "0.0.0",
        product_archetype: ""
    }
};
describe("Hue-Sync", function() {
    afterEach(function() {
        fetchMock.resetMocks();
    });
    describe("static methods", function() {
        var mockBridgeNetworkDevice = {
            id: "foo",
            port: 123,
            internalipaddress: "bar"
        };
        it("should be able to discover Hue Bridge devices via mDNS", /*#__PURE__*/ _asyncToGenerator(function() {
            var _tmp, ref, bridgeOnNetwork;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        mdnsMock.discover.mockReturnValueOnce([
                            (_tmp.address = mockBridgeNetworkDevice.internalipaddress, _tmp.service = {
                                port: mockBridgeNetworkDevice.port
                            }, _tmp.packet = {
                                additionals: [
                                    {
                                        rdata: {
                                            bridgeid: mockBridgeNetworkDevice.id
                                        }
                                    }, 
                                ]
                            }, _tmp)
                        ]);
                        return [
                            4,
                            HueBridge.discover()
                        ];
                    case 1:
                        ref = _slicedToArray.apply(void 0, [
                            _state.sent(),
                            1
                        ]), bridgeOnNetwork = ref[0];
                        expect(bridgeOnNetwork).toEqual(mockBridgeNetworkDevice);
                        expect(mdnsMock.discover.mock.calls.length).toBe(1);
                        return [
                            2
                        ];
                }
            });
        }));
        it("should be able to discover Hue Bridge devices via remote API", /*#__PURE__*/ _asyncToGenerator(function() {
            var mockBridgeNetworkDevice, _tmp, ref, bridgeOnNetwork;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        mockBridgeNetworkDevice = (_tmp.id = "foo", _tmp.port = 123, _tmp.internalipaddress = "bar", _tmp);
                        mdnsMock.discover.mockRejectedValueOnce(null);
                        fetchMock.mockOnce(JSON.stringify([
                            mockBridgeNetworkDevice
                        ]));
                        return [
                            4,
                            HueBridge.discover()
                        ];
                    case 1:
                        ref = _slicedToArray.apply(void 0, [
                            _state.sent(),
                            1
                        ]), bridgeOnNetwork = ref[0];
                        expect(bridgeOnNetwork).toEqual(mockBridgeNetworkDevice);
                        return [
                            2
                        ];
                }
            });
        }));
        it("should be able to register hue-sync on Hue Bridge device", /*#__PURE__*/ _asyncToGenerator(function() {
            var _tmp, credentials;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        fetchMock.mockOnce(JSON.stringify([
                            (_tmp.success = mockCredentials, _tmp)
                        ]));
                        return [
                            4,
                            HueBridge.register(mockIp)
                        ];
                    case 1:
                        credentials = _state.sent();
                        expect(credentials).toEqual(mockCredentials);
                        return [
                            2
                        ];
                }
            });
        }));
    });
    describe("instance methods", function() {
        var bridge = new HueBridge({
            id: mockID,
            url: mockIp,
            credentials: mockCredentials
        });
        describe("Create Methods", function() {
            it("should be able to add an Entertainment Area", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockEntertainmentArea
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.addEntertainmentArea((_tmp1.metadata = {
                                    name: "test"
                                }, _tmp1.configuration_type = "test", _tmp1.locations = {
                                    service_locations: [
                                        {
                                            service: mockResourceNode,
                                            position: {
                                                x: 1,
                                                y: 2,
                                                z: 3
                                            },
                                            positions: [
                                                {
                                                    x: 1,
                                                    y: 2,
                                                    z: 3
                                                },
                                                {
                                                    x: 4,
                                                    y: 5,
                                                    z: 6
                                                }, 
                                            ]
                                        }, 
                                    ]
                                }, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockEntertainmentArea);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to add a new Scene", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockScene
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.addScene((_tmp1.metadata = {
                                    name: "bar"
                                }, _tmp1.group = mockResourceNode, _tmp1.actions = [
                                    {
                                        target: mockResourceNode,
                                        action: {
                                            on: {
                                                on: true
                                            }
                                        }
                                    }, 
                                ], _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockScene);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to add a new Room", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockRoom
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.addRoom((_tmp1.metadata = {
                                    name: "foo"
                                }, _tmp1.children = [
                                    mockResourceNode
                                ], _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockRoom);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to add a new Zone", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockZone
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.addZone((_tmp1.children = [
                                    mockResourceNode
                                ], _tmp1.metadata = {
                                    name: "foo"
                                }, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockZone);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to add a new Geo Fence Client", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockGeoFenceClient
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.addGeoFenceClient((_tmp1.name = "foo", _tmp1.type = "test", _tmp1.is_at_home = false, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockGeoFenceClient);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to add a new Behavior Instance", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockBehaviorInstance
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.addBehaviorInstance((_tmp1.enabled = true, _tmp1.script_id = "test", _tmp1.type = "test", _tmp1.metadata = {
                                    name: "baz"
                                }, _tmp1.configuration = {}, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockBehaviorInstance);
                            return [
                                2
                            ];
                    }
                });
            }));
        });
        describe("Read Methods", function() {
            beforeEach(function() {
                bridge = new HueBridge({
                    id: mockID,
                    url: mockIp,
                    credentials: mockCredentials
                });
                fetchMock.resetMocks();
            });
            it("should get all Lights", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockLight,
                                secondMockLight
                            ], _tmp)));
                            return [
                                4,
                                bridge.getLights()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(2);
                            expect(result[0].id).toEqual(mockLight.id);
                            expect(result[1].id).toEqual(secondMockLight.id);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get all Light Groups", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockLightGroup,
                                mockLightGroup2
                            ], _tmp)));
                            return [
                                4,
                                bridge.getLightGroups()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(2);
                            expect(result[0].id).toEqual(mockLightGroup.id);
                            expect(result[1].id).toEqual(mockLightGroup2.id);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve all Scenes", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockScene,
                                mockScene
                            ], _tmp)));
                            return [
                                4,
                                bridge.getScenes()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(2);
                            expect(result[0]).toEqual(mockScene);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve all Rooms", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockRoom,
                                mockRoom
                            ], _tmp)));
                            return [
                                4,
                                bridge.getRooms()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(2);
                            expect(result[0]).toEqual(mockRoom);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve all Zones", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockZone,
                                mockZone
                            ], _tmp)));
                            return [
                                4,
                                bridge.getZones()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(2);
                            expect(result[0]).toEqual(mockZone);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get all the Home Areas", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockHomeArea
                            ], _tmp)));
                            return [
                                4,
                                bridge.getHomeAreas()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(1);
                            expect(result[0]).toEqual(mockHomeArea);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get all the Entertainment Areas", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockEntertainmentArea
                            ], _tmp)));
                            return [
                                4,
                                bridge.getEntertainmentAreas()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(1);
                            expect(result[0]).toEqual(mockEntertainmentArea);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve All Geo Fence Clients", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockGeoFenceClient,
                                mockGeoFenceClient
                            ], _tmp)));
                            return [
                                4,
                                bridge.getAllGeoFenceClients()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(2);
                            expect(result[0]).toEqual(mockGeoFenceClient);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve All Behavior Instances", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockBehaviorInstance,
                                mockBehaviorInstance
                            ], _tmp)));
                            return [
                                4,
                                bridge.getAllBehaviorInstances()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(2);
                            expect(result[0]).toEqual(mockBehaviorInstance);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get all registered Devices", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockDevice,
                                mockDevice2
                            ], _tmp)));
                            return [
                                4,
                                bridge.getDevices()
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result.length).toBe(2);
                            expect(result[0].id).toEqual(mockDevice.id);
                            expect(result[1].id).toEqual(mockDevice2.id);
                            return [
                                2
                            ];
                    }
                });
            }));
            // individuals
            it("should be able to retrieve Hue Bridge config information", /*#__PURE__*/ _asyncToGenerator(function() {
                var mockConfig, _tmp, config;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            mockConfig = (_tmp.name = "foo", _tmp.datastoreversion = "bar", _tmp.swversion = "baz", _tmp.apiversion = "lorem", _tmp.mac = "ipsum", _tmp.bridgeid = "dolor", _tmp.factorynew = false, _tmp.replacesbridgeid = "amet", _tmp.modelid = "consecutir", _tmp.starterkitid = "dolor", _tmp);
                            fetchMock.mockOnce(JSON.stringify(mockConfig));
                            return [
                                4,
                                bridge.getInfo()
                            ];
                        case 1:
                            config = _state.sent();
                            expect(config).toEqual(mockConfig);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get a specific Light", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockLight
                            ], _tmp)));
                            return [
                                4,
                                bridge.getLight(mockLight.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockLight);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get a specific Light Group", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockLightGroup
                            ], _tmp)));
                            return [
                                4,
                                bridge.getLightGroup(mockLightGroup.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockLightGroup);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve a specific Scene", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockScene
                            ], _tmp)));
                            return [
                                4,
                                bridge.getScene(mockScene.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockScene);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve a specific Room", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockRoom
                            ], _tmp)));
                            return [
                                4,
                                bridge.getRoom(mockRoom.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockRoom);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve a specific Zone", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockZone
                            ], _tmp)));
                            return [
                                4,
                                bridge.getZone(mockZone.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockZone);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get a specific Home Area", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockHomeArea
                            ], _tmp)));
                            return [
                                4,
                                bridge.getHomeArea(mockHomeArea.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockHomeArea);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get a specific Entertainment Area", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockEntertainmentArea
                            ], _tmp)));
                            return [
                                4,
                                bridge.getEntertainmentArea(mockEntertainmentArea.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockEntertainmentArea);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve a specific Geo Fence Client", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockGeoFenceClient
                            ], _tmp)));
                            return [
                                4,
                                bridge.getGeoFenceClient(mockGeoFenceClient.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockGeoFenceClient);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to retrieve a specific Behavior Instance", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockBehaviorInstance
                            ], _tmp)));
                            return [
                                4,
                                bridge.getBehaviorInstance(mockBehaviorInstance.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockBehaviorInstance);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should get a specific registered Device", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockDevice
                            ], _tmp)));
                            return [
                                4,
                                bridge.getDevice(mockDevice.id)
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockDevice);
                            return [
                                2
                            ];
                    }
                });
            }));
        });
        describe("Update Methods", function() {
            beforeEach(function() {
                bridge = new HueBridge({
                    id: mockID,
                    url: mockIp,
                    credentials: mockCredentials
                });
                fetchMock.resetMocks();
            });
            it("should update a single light", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateLight(mockLight.id, (_tmp1.on = {
                                    on: true
                                }, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a grouped light", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateLightGroup(mockLightGroup.id, (_tmp1.on = {
                                    on: true
                                }, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a scene", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateScene(mockScene.id, (_tmp1.speed = 0, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a room", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateRoom(mockRoom.id, (_tmp1.services = [
                                    mockResourceNode
                                ], _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a zone", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateZone(mockZone.id, (_tmp1.services = [
                                    mockResourceNode
                                ], _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a Home Area", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateHomeArea(mockHomeArea.id, (_tmp1.services = [
                                    mockResourceNode
                                ], _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a given entertainment area", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateEntertainmentArea(mockEntertainmentArea.id, (_tmp1.action = "stop", _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a Geo Fence Client", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateGeoFenceClient(mockGeoFenceClient.id, (_tmp1.name = "foo", _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a device", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateDevice(mockDevice.id, (_tmp1.metadata = {
                                    archetype: "unknown_archetype"
                                }, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should update a behavior instance", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, result, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            _tmp1 = {};
                            return [
                                4,
                                bridge.updateBehaviorInstance(mockBehaviorInstance.id, (_tmp1.enabled = true, _tmp1))
                            ];
                        case 1:
                            result = _state.sent();
                            expect(result).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
        });
        describe("Delete Methods", function() {
            it("should be able to remove a Scene by ID", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, actualResult;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            return [
                                4,
                                bridge.removeScene(mockResourceNode.rid)
                            ];
                        case 1:
                            actualResult = _state.sent();
                            expect(actualResult).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to remove a Room by ID", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, actualResult;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            return [
                                4,
                                bridge.removeRoom(mockResourceNode.rid)
                            ];
                        case 1:
                            actualResult = _state.sent();
                            expect(actualResult).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to remove a Zone by ID", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, actualResult;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            return [
                                4,
                                bridge.removeZone(mockResourceNode.rid)
                            ];
                        case 1:
                            actualResult = _state.sent();
                            expect(actualResult).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to remove an Entertainment Area by ID", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, actualResult;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            return [
                                4,
                                bridge.removeEntertainmentArea(mockResourceNode.rid)
                            ];
                        case 1:
                            actualResult = _state.sent();
                            expect(actualResult).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to remove a Geo Fence Client by ID", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, actualResult;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            return [
                                4,
                                bridge.removeGeoFenceClient(mockResourceNode.rid)
                            ];
                        case 1:
                            actualResult = _state.sent();
                            expect(actualResult).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to remove a Behavior Instance by ID", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, actualResult;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            return [
                                4,
                                bridge.removeBehaviorInstance(mockResourceNode.rid)
                            ];
                        case 1:
                            actualResult = _state.sent();
                            expect(actualResult).toEqual(mockResourceNode);
                            return [
                                2
                            ];
                    }
                });
            }));
        });
        describe("Streaming Entertainment API", function() {
            it("should throw when calling stop with no active channel", function() {
                try {
                    bridge.stop();
                } catch (e) {
                    expect(e.message).toBe("No active datagram socket!");
                }
            });
            it("should throw when calling transition with no active channel", /*#__PURE__*/ _asyncToGenerator(function() {
                var e;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _state.trys.push([
                                0,
                                2,
                                ,
                                3
                            ]);
                            return [
                                4,
                                bridge.transition([
                                    justGreen
                                ])
                            ];
                        case 1:
                            _state.sent();
                            return [
                                3,
                                3
                            ];
                        case 2:
                            e = _state.sent();
                            expect(e.message).toBe("No active datagram socket!");
                            return [
                                3,
                                3
                            ];
                        case 3:
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should setup a dgram channel for a given entertainment area", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, _tmp1;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockEntertainmentArea
                            ], _tmp)));
                            _tmp1 = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp1.data = [
                                mockResourceNode
                            ], _tmp1)));
                            return [
                                4,
                                bridge.start(mockEntertainmentArea)
                            ];
                        case 1:
                            _state.sent();
                            // @ts-ignore
                            expect(bridge.socket).toBeDefined();
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to transmit an RGB array through Hue Entertainment API", /*#__PURE__*/ _asyncToGenerator(function() {
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            return [
                                4,
                                bridge.transition([
                                    justGreen
                                ])
                            ];
                        case 1:
                            _state.sent();
                            // @ts-ignore
                            expect(bridge.socket.send.mock.calls.length).toBe(1);
                            // @ts-ignore
                            expect(bridge.socket.send.mock.calls[0][0]).toBeInstanceOf(Buffer);
                            return [
                                2
                            ];
                    }
                });
            }));
            it("should be able to close the dgram channel for an active entertainment area", /*#__PURE__*/ _asyncToGenerator(function() {
                var _tmp, abortSpy;
                return __generator(this, function(_state) {
                    switch(_state.label){
                        case 0:
                            _tmp = {};
                            fetchMock.mockOnce(JSON.stringify((_tmp.data = [
                                mockResourceNode
                            ], _tmp)));
                            abortSpy = jest.spyOn(AbortController.prototype, "abort");
                            return [
                                4,
                                bridge.stop()
                            ];
                        case 1:
                            _state.sent();
                            expect(abortSpy.mock.calls.length).toBe(1);
                            return [
                                2
                            ];
                    }
                });
            }));
        });
    });
});
