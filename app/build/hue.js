function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length) len = arr.length;
    for(var i = 0, arr2 = new Array(len); i < len; i++)arr2[i] = arr[i];
    return arr2;
}
function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
}
function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
        var info = gen[key](arg);
        var value = info.value;
    } catch (error) {
        reject(error);
        return;
    }
    if (info.done) {
        resolve(value);
    } else {
        Promise.resolve(value).then(_next, _throw);
    }
}
function _asyncToGenerator(fn) {
    return function() {
        var self = this, args = arguments;
        return new Promise(function(resolve, reject) {
            var gen = fn.apply(self, args);
            function _next(value) {
                asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
            }
            function _throw(err) {
                asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
            }
            _next(undefined);
        });
    };
}
function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}
function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, {
            value: value,
            enumerable: true,
            configurable: true,
            writable: true
        });
    } else {
        obj[key] = value;
    }
    return obj;
}
function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}
function _iterableToArrayLimit(arr, i) {
    var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];
    if (_i == null) return;
    var _arr = [];
    var _n = true;
    var _d = false;
    var _s, _e;
    try {
        for(_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true){
            _arr.push(_s.value);
            if (i && _arr.length === i) break;
        }
    } catch (err) {
        _d = true;
        _e = err;
    } finally{
        try {
            if (!_n && _i["return"] != null) _i["return"]();
        } finally{
            if (_d) throw _e;
        }
    }
    return _arr;
}
function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}
function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}
function _unsupportedIterableToArray(o, minLen) {
    if (!o) return;
    if (typeof o === "string") return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor) n = o.constructor.name;
    if (n === "Map" || n === "Set") return Array.from(n);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
var __generator = this && this.__generator || function(thisArg, body) {
    var f, y, t, g, _ = {
        label: 0,
        sent: function() {
            if (t[0] & 1) throw t[1];
            return t[1];
        },
        trys: [],
        ops: []
    };
    return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
    }, typeof Symbol === "function" && (g[Symbol.iterator] = function() {
        return this;
    }), g;
    function verb(n) {
        return function(v) {
            return step([
                n,
                v
            ]);
        };
    }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while(_)try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [
                op[0] & 2,
                t.value
            ];
            switch(op[0]){
                case 0:
                case 1:
                    t = op;
                    break;
                case 4:
                    _.label++;
                    return {
                        value: op[1],
                        done: false
                    };
                case 5:
                    _.label++;
                    y = op[1];
                    op = [
                        0
                    ];
                    continue;
                case 7:
                    op = _.ops.pop();
                    _.trys.pop();
                    continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                        _ = 0;
                        continue;
                    }
                    if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                        _.label = op[1];
                        break;
                    }
                    if (op[0] === 6 && _.label < t[1]) {
                        _.label = t[1];
                        t = op;
                        break;
                    }
                    if (t && _.label < t[2]) {
                        _.label = t[2];
                        _.ops.push(op);
                        break;
                    }
                    if (t[2]) _.ops.pop();
                    _.trys.pop();
                    continue;
            }
            op = body.call(thisArg, _);
        } catch (e) {
            op = [
                6,
                e
            ];
            y = 0;
        } finally{
            f = t = 0;
        }
        if (op[0] & 5) throw op[1];
        return {
            value: op[0] ? op[1] : void 0,
            done: true
        };
    }
};
import mdns from "node-dns-sd";
import { dtls } from "node-dtls-client";
if (!globalThis.fetch) {
    require("cross-fetch/polyfill");
}
var patchDNS = function(domain, ip) {
    var dns = require("dns");
    var query = new RegExp(domain, "i");
    var originalLookup = dns.lookup;
    var newLookup = function(domain, options, callback) {
        if (query.test(domain)) {
            return callback(null, ip, 4);
        }
        return originalLookup(domain, options, callback);
    };
    dns.lookup = newLookup;
};
var HueBridge = /*#__PURE__*/ function() {
    "use strict";
    function HueBridge(initial) {
        _classCallCheck(this, HueBridge);
        // properties
        this.id = null;
        this.url = null;
        this.socket = null;
        this.abortionController = null;
        this.entertainmentArea = null;
        this.credentials = null;
        this.id = initial.id;
        this.url = initial.url;
        this.credentials = initial.credentials;
        patchDNS(this.id, this.url);
    }
    var _proto = HueBridge.prototype;
    _proto._request = function _request(endpoint) {
        var options = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : {
            headers: {},
            method: "GET"
        };
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        if (!options.headers) {
                            options.headers = {};
                        }
                        if (options.body && options.method !== "GET") {
                            options.body = JSON.stringify(options.body);
                            options.headers["Content-Type"] = "application/json";
                        }
                        options.keepAlive = true;
                        options.headers["hue-application-key"] = _this.credentials.username;
                        return [
                            4,
                            fetch(endpoint, options)
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            response.json()
                        ];
                }
            });
        })();
    };
    _proto._unwrap = function _unwrap(param) {
        var errors = param.errors, data = param.data;
        if (!errors || errors.length === 0) {
            return data;
        }
        throw errors[0];
    };
    // Datagram streaming
    _proto.start = function start(selectedArea) {
        var timeout = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : 1000;
        var _this = this;
        return _asyncToGenerator(function() {
            var _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _this.entertainmentArea = selectedArea;
                        _this.abortionController = new AbortController();
                        _tmp = {};
                        return [
                            4,
                            _this.updateEntertainmentArea(selectedArea.id, (_tmp.action = "start", _tmp))
                        ];
                    case 1:
                        _state.sent();
                        _this.socket = dtls.createSocket({
                            timeout: timeout,
                            port: 2100,
                            type: "udp4",
                            address: _this.url,
                            signal: _this.abortionController.signal,
                            cipherSuites: [
                                "TLS_PSK_WITH_AES_128_GCM_SHA256"
                            ],
                            psk: _defineProperty({}, _this.credentials.username, Buffer.from(_this.credentials.clientkey, "hex"))
                        });
                        return [
                            2,
                            new Promise(function(resolve) {
                                return _this.socket.on("connected", resolve);
                            })
                        ];
                }
            });
        })();
    };
    _proto.stop = function stop() {
        var _this = this;
        if (!this.socket) {
            throw new Error("No active datagram socket!");
        }
        var id = this.entertainmentArea.id;
        this.socket.on("close", function() {
            _this.updateEntertainmentArea(id, {
                action: "stop"
            });
        });
        this.abortionController.abort();
        this.entertainmentArea = null;
        this.abortionController = null;
        this.socket = null;
    };
    // #NOTE: one [R,G,B] per channel
    _proto.transition = function transition(colors) {
        if (!this.socket) {
            throw new Error("No active datagram socket!");
        }
        var protocol = Buffer.from("HueStream");
        var version = Buffer.from([
            0x02,
            0x00
        ]); // V2.0
        var sequenceNumber = Buffer.from([
            0x00
        ]); // currently ignored
        var reservedSpaces = Buffer.from([
            0x00,
            0x00
        ]);
        var colorMode = Buffer.from([
            0x00
        ]); // 0 = RGB, 1 = XY
        var reservedSpace = Buffer.from([
            0x00
        ]);
        var groupId = Buffer.from(this.entertainmentArea.id);
        var rgbChannels = colors.map(function(rgb, channelIndex) {
            return Buffer.from([
                channelIndex,
                rgb[0],
                rgb[0],
                rgb[1],
                rgb[1],
                rgb[2],
                rgb[2]
            ]);
        });
        var message = Buffer.concat([
            protocol,
            version,
            sequenceNumber,
            reservedSpaces,
            colorMode,
            reservedSpace,
            groupId, 
        ].concat(_toConsumableArray(rgbChannels)));
        this.socket.send(message);
    };
    // Create
    _proto.addScene = function addScene(data) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/scene"), (_tmp.body = data, _tmp.method = "POST", _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.addRoom = function addRoom(data) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/room"), (_tmp.method = "POST", _tmp.body = data, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.addZone = function addZone(data) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/zone"), (_tmp.method = "POST", _tmp.body = data, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.addEntertainmentArea = function addEntertainmentArea(data) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/entertainment_configuration"), (_tmp.method = "POST", _tmp.body = data, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.addGeoFenceClient = function addGeoFenceClient(data) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/geofence_client"), (_tmp.method = "POST", _tmp.body = data, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.addBehaviorInstance = function addBehaviorInstance(data) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/behavior_instance"), (_tmp.method = "POST", _tmp.body = data, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    // Read
    _proto.getInfo = function getInfo() {
        return this._request("https://".concat(this.id, "/api/0/config"));
    };
    _proto.getLights = function getLights() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/light"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getLight = function getLight(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/light/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getLightGroups = function getLightGroups() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/grouped_light"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getLightGroup = function getLightGroup(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/grouped_light/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getScenes = function getScenes() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/scene"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getScene = function getScene(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/scene/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getRooms = function getRooms() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/room"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getRoom = function getRoom(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/room/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getZones = function getZones() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/zone"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getZone = function getZone(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/zone/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getEntertainmentAreas = function getEntertainmentAreas() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/entertainment_configuration"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getEntertainmentArea = function getEntertainmentArea(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/entertainment_configuration/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getHomeAreas = function getHomeAreas() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/bridge_home"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getHomeArea = function getHomeArea(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/bridge_home/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getDevices = function getDevices() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/device"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getDevice = function getDevice(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/device/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getAllGeoFenceClients = function getAllGeoFenceClients() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/geofence_client"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getGeoFenceClient = function getGeoFenceClient(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/geofence_client/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.getAllBehaviorInstances = function getAllBehaviorInstances() {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/behavior_instance"))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)
                        ];
                }
            });
        })();
    };
    _proto.getBehaviorInstance = function getBehaviorInstance(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/behavior_instance/").concat(id))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    // types not implemented yet
    // getEntertainmentServices() {}
    // getEntertainmentService(id: string) {}
    // getMotionServices() {}
    // getMotionService(id: string) {}
    // getPowerDeviceServices() {}
    // getPowerDeviceService(id: string) {}
    // getTemperatureServices() {}
    // getTemperatureService(id: string) {}
    // getLightLevelServices() {}
    // getLightLevelService(id: string) {}
    // getButtonServices() {}
    // getButtonService(id: string) {}
    // getGeolocationServices() {}
    // getGeolocationService(id: string) {}
    // getHomeKitServices() {}
    // getHomeKitService(id: string) {}
    // getZigbeeConnectivityServices() {}
    // getZigbeeConnectivityService(id: string) {}
    // getZigbeeGreenPowerServices() {}
    // getZigbeeGreenPowerService(id: string) {}
    // getAllBehaviorScripts() {}
    // getBehaviorScript(id: string) {}
    // Update
    _proto.updateEntertainmentArea = function updateEntertainmentArea(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/entertainment_configuration/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateLight = function updateLight(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/light/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateScene = function updateScene(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/scene/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateRoom = function updateRoom(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/room/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateZone = function updateZone(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/zone/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateHomeArea = function updateHomeArea(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/bridge_home/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateLightGroup = function updateLightGroup(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/grouped_light/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateDevice = function updateDevice(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/device/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateBehaviorInstance = function updateBehaviorInstance(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/behavior_instance/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.updateGeoFenceClient = function updateGeoFenceClient(id, updates) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/geofecne_client/").concat(id), (_tmp.method = "PUT", _tmp.body = updates, _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    // types not implemented yet
    /*
  async updateMotionService(id: string, updates: {}): Promise<ResourceNode> {}
  async updateTemperatureService(id: string, updates: {}): Promise<ResourceNode> {}
  async updateLightLevelService(id: string, updates: {}): Promise<ResourceNode> {}
  async updateButtonService(id: string, updates: {}): Promise<ResourceNode> {}
  async updateGeoLocationService(id: string, updates: {}): Promise<ResourceNode> {}
  async updateEntertainmentService(id: string, updates: {}): Promise<ResourceNode> {}
  async updateHomeKitService(id: string, updates: {}): Promise<ResourceNode> {}
  async updateDevicePowerService(id: string, updates: {}): Promise<ResourceNode> {}
  async updateZigbeeConnectivityService(
    id: string,
    update: {}
  ): Promise<ResourceNode> {}
  async updateZigbeeGreenPowerService(
    id: string,
    updates: {}
  ): Promise<ResourceNode> {}
  */ // Delete
    _proto.removeScene = function removeScene(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/scene/").concat(id), (_tmp.method = "DELETE", _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.removeRoom = function removeRoom(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/room/").concat(id), (_tmp.method = "DELETE", _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.removeZone = function removeZone(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/zone/").concat(id), (_tmp.method = "DELETE", _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.removeBehaviorInstance = function removeBehaviorInstance(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/behavior_instance/").concat(id), (_tmp.method = "DELETE", _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.removeGeoFenceClient = function removeGeoFenceClient(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/geofence_client/").concat(id), (_tmp.method = "DELETE", _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    _proto.removeEntertainmentArea = function removeEntertainmentArea(id) {
        var _this = this;
        return _asyncToGenerator(function() {
            var response, _tmp;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _tmp = {};
                        return [
                            4,
                            _this._request("https://".concat(_this.id, "/clip/v2/resource/entertainment_configuration/").concat(id), (_tmp.method = "DELETE", _tmp))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            2,
                            _this._unwrap(response)[0]
                        ];
                }
            });
        })();
    };
    HueBridge.discover = function discover() {
        return _asyncToGenerator(function() {
            var localSearch, _tmp, e, response;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        _state.trys.push([
                            0,
                            2,
                            ,
                            4
                        ]);
                        _tmp = {};
                        return [
                            4,
                            mdns.discover((_tmp.name = "_hue._tcp.local", _tmp))
                        ];
                    case 1:
                        localSearch = _state.sent();
                        return [
                            2,
                            localSearch.map(function(item) {
                                var port = item.service.port;
                                var internalipaddress = item.address;
                                var _additionals = _slicedToArray(item.packet.additionals, 1), buffer = _additionals[0];
                                var id = buffer.rdata.bridgeid;
                                return {
                                    id: id,
                                    port: port,
                                    internalipaddress: internalipaddress
                                };
                            })
                        ];
                    case 2:
                        e = _state.sent();
                        return [
                            4,
                            fetch("https://discovery.meethue.com/")
                        ];
                    case 3:
                        response = _state.sent();
                        return [
                            2,
                            response.json()
                        ];
                    case 4:
                        return [
                            2
                        ];
                }
            });
        })();
    };
    HueBridge.register = function register(url) {
        var devicetype = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : "hue-sync";
        return _asyncToGenerator(function() {
            var endpoint, body, _tmp, response, _tmp1, ref, ref1, error, success;
            return __generator(this, function(_state) {
                switch(_state.label){
                    case 0:
                        endpoint = "http://".concat(url, "/api");
                        _tmp = {};
                        body = JSON.stringify((_tmp.devicetype = devicetype, _tmp.generateclientkey = true, _tmp));
                        _tmp1 = {};
                        return [
                            4,
                            fetch(endpoint, (_tmp1.body = body, _tmp1.method = "POST", _tmp1.headers = {
                                "Content-Type": "application/json"
                            }, _tmp1))
                        ];
                    case 1:
                        response = _state.sent();
                        return [
                            4,
                            response.json()
                        ];
                    case 2:
                        ref = _slicedToArray.apply(void 0, [
                            _state.sent(),
                            1
                        ]), ref1 = ref[0], error = ref1.error, success = ref1.success;
                        if (error) throw error;
                        return [
                            2,
                            success
                        ];
                }
            });
        })();
    };
    return HueBridge;
}();
export { HueBridge as default };
