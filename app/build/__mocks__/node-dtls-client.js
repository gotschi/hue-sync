export var dtls = {
    createSocket: jest.fn(function() {
        return {
            send: jest.fn(),
            on: jest.fn(function(event, callback) {
                if (event === "connected" || event === "close") callback();
            })
        };
    })
};
